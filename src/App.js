import React from 'react';
import logo from './logo.svg';
import './App.css';
import './components/employees/Employee';

function App() {
  return (
    <div className="App">
      <ul>
        <li><a href="/employees">Employees</a></li>
        <li><a href="/employee-details">Employee Details</a></li>
      </ul>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
    </div>
  );
}

export default App;
