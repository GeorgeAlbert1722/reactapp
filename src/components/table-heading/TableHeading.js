import React from 'react';

class TableHeading extends React.Component {
    render() {
        return (
            <thead>
                <tr>
                    <th>Company</th>
                    <th>Contact</th>
                    <th>Country</th>
                    <th>Action</th>
                </tr>
            </thead>
        );
    }
}
export default TableHeading;