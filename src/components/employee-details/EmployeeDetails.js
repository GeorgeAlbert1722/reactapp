import React from 'react';
import './EmployeeDetails.css'
import TableHeading from '../table-heading/TableHeading';
import TableBody from '../table-body/TableBody'

class EmployeeDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      details: [
        {
          company: 'Alfreds Futterkiste',
          contact: 'Maria Anders',
          country: 'Germany'
        },
        {
          company: 'Centro comercial Moctezuma',
          contact: 'Francisco Chang',
          country: 'Mexico'
        },
        {
          company: 'Ernst Handel',
          contact: 'Roland Mendel',
          country: 'Austria'
        },
        {
          company: 'Island Trading',
          contact: 'Helen Bennett',
          country: 'UK'
        },
        {
          company: 'Laughing Bacchus Winecellars',
          contact: 'Yoshi Tannamuri',
          country: 'Canada'
        },
        {
          company: 'Magazzini Alimentari Riuniti',
          contact: 'Giovanni Rovelli',
          country: 'Italy'
        }
      ]
    }
  }
  rout = () => {
    this.props.history.push('/employees');
  }

  callbackFunction = (key) => {
    const temp = this.state.details;
    delete temp[key];
    this.setState({ details: temp });
  }

  header = () => {
    if(this.props.location.state) {
      return (
        <div className="row">
          <div className="column">
            <img src={this.props.location.state.url} onClick={this.rout} alt = {this.props.location.state.name}/>
          </div>
          <div className="column">
            {this.props.location.state.name}
          </div>
        </div>
      );
    }
  }

  render() {
    return (
      <div>
      {
        this.header()
      }
        <table>
          <TableHeading />
          <tbody>
            {
              this.state.details.map((data, index) => {
                return (
                  <TableBody key={index.toString()} index={index} value={data} parentCallback={this.callbackFunction} />
                );
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}
export default EmployeeDetail;