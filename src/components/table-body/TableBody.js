import React from 'react'

class TableBody extends React.Component {
    constructor(props) {
        super(props)
    }
    sendData = (data) => {
        this.props.parentCallback(data)
    }
    render() {
        return (
            <tr>
                <td>{this.props.value.company}</td>
                <td>{this.props.value.contact}</td>
                <td>{this.props.value.country}</td>
                <td><button onClick = {() => this.sendData(this.props.index)}>Delete</button></td>
            </tr>
        );
    }
}
export default TableBody;