import React from 'react';
import './Employee.css'

class Employee extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            employees: [
                {
                    url: 'https://www.irishlifecorporatebusiness.ie/sites/default/files/slider/employee_2.jpg',
                    name: 'Allen',
                    doj: '01-01-2020'
                },
                {
                    url: 'https://cdn.shrm.org/image/upload/c_crop,h_705,w_1254,x_0,y_15/c_fit,f_auto,q_auto,w_767/v1/Legal%20and%20Compliance/reviewing_paperwork_and_on_computer_pierld?databtoa=eyIxNng5Ijp7IngiOjAsInkiOjE1LCJ4MiI6MTI1NCwieTIiOjcyMCwidyI6MTI1NCwiaCI6NzA1fX0%3D',
                    name: 'John',
                    doj: '01-02-2020'
                },
                {
                    url: 'https://www.servicenow.com/content/dam/servicenow-assets/public/en-us/images/stock-photography/hr/usecase-marquee-hr.jpg',
                    name: 'Christophe',
                    doj: '01-03-2020'
                }
            ]
        }
    }
    rout = (data) => {
        this.props.history.push('/employee-details', data);
    }
    render() {
        return (
            <div>
                <h1>Employees</h1>
                <div className="row">
                    {
                        this.state.employees.map((data, index) => {
                            return (
                                <div key={index.toString()} className="column">
                                    <img src={data.url} onClick={() => this.rout(data)} alt={data.name} />
                                    <p>{data.name}</p>
                                    <p>{data.doj}</p>
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}
export default Employee;